<h1 align="center">TESTE TÉCNICO IMDb API</h1>
<div align="center">
  <img width="auto" height="23em" src="https://img.shields.io/badge/TypeScript-323330?style=flat&logo=TypeScript">
  <img width="auto" height="23em" src="https://img.shields.io/badge/Node.js-323330?style=flat&logo=Node.js">
  <img width="auto" height="23em" src="https://img.shields.io/badge/Express.js-323330?style=flat&logo=express">
  <img width="auto" height="23em" src="https://img.shields.io/badge/MySQL-323330?style=flate&logo=mysql">
  <img width="auto" height="23em" src="https://img.shields.io/badge/Jest-323330?style=flat&logo=jest&logoColor=99424F">
  <img width="auto" height="23em" src="https://img.shields.io/badge/Docker-323330?style=flat&logo=docker&logoColor=99424F">
</div>

## Pré-Requisitos

- NodeJS (v18.16.0) e npm (v9.5.1).
- MySQL (v8.0).
