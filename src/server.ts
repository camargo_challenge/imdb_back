import http from 'http';

import app from './app';
import DataSource from '@database/data-source';

DataSource.initialize()
 .then(async () => {
  const server = http.createServer(app);

  server.listen(process.env.PORT || 3333, () => {
   console.log(`API IMDb iniciada na porta ${process.env.PORT || 3333}`);
   console.log(`Ambiente: ${process.env.NODE_ENV}`);
   console.log(
    `Documentação: http://localhost:${process.env.PORT || 3333}/docs`,
   );
  });
 })
 .catch(err => {
  console.error(err);
 });
