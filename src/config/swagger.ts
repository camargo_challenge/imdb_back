export default {
 definition: {
  openapi: '3.0.0',
  info: {
   title: 'Challenge IMDb REST API Documentation',
   version: '1.0.0',
  },
  servers: [
   {
    url: '/',
    description: 'Local environment',
   },
   {
    url: '/api',
    description: 'Production environment',
   },
  ],
 },
 apis: ['docs/**/*.yml'],
};
