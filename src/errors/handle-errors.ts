import AppException from './app-exception';

export function handleErrors(error: any, message?: string) {
 throw new AppException(error.message ?? message ?? 500, error.status ?? 500);
}
