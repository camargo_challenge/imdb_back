import UserErrorsMessages from './user-errors.messages';
import AuthErrorsMessages from './auth-errors.messages';
import MovieErrorsMessages from './movie-errors.messages';
import GenreErrorsMessages from './genre-errors.messages';

export default {
 INTERNAL_SERVER_ERROR: 'Erro interno no servidor.',
 UNATHORIZED: 'Você precisa estar autenticado para prosseguir.',
 FORBIDDEN: 'Sem permissão para acessar esse recurso.',
 ...UserErrorsMessages,
 ...AuthErrorsMessages,
 ...MovieErrorsMessages,
 ...GenreErrorsMessages,
};
