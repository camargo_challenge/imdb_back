export default {
 USER_NOT_FOUND: 'O usuário não foi encontrado',
 USER_ALREADY_EXISTS: 'O usuário já existe',
 USER_INACTIVE: 'O usuário está inativo',
};
