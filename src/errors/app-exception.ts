class AppException extends Error {
 public status;
 public message;

 constructor(message: string, status: number) {
  super(message);
  this.status = status;
  this.message = message;
 }
}

export default AppException;
