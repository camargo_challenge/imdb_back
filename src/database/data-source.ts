import 'dotenv/config';
import { join as path } from 'path';
import { DataSource } from 'typeorm';

export default new DataSource({
 type: process.env.TYPEORM_CONNECTION as 'mysql' | 'postgres',
 host: process.env.TYPEORM_HOST,
 port: parseInt(process.env.TYPEORM_PORT as string),
 username: process.env.TYPEORM_USERNAME,
 password: process.env.TYPEORM_PASSWORD,
 database: process.env.TYPEORM_DATABASE_NAME,
 synchronize: false,
 logging: false,
 migrationsRun: true,
 entities: [path(__dirname, '../app/**/*.model{.ts,.js}')],
 migrations: [path(__dirname, '../database/migrations/*{.ts,.js}')],
 timezone: '-03:00',
});
