import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUserBaseTable1702329052658 implements MigrationInterface {
 name = 'CreateUserBaseTable1702329052658';

 public async up(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `CREATE TABLE \`users\` (\`id\` varchar(36) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, \`name\` varchar(255) NULL, \`email\` varchar(255) NOT NULL, \`image\` varchar(255) NULL, \`image_key\` varchar(255) NULL, \`password\` varchar(255) NULL, \`status\` enum ('ativo', 'inativo') NOT NULL DEFAULT 'ativo', UNIQUE INDEX \`IDX_97672ac88f789774dd47f7c8be\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
  );
 }

 public async down(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `DROP INDEX \`IDX_97672ac88f789774dd47f7c8be\` ON \`users\``,
  );
  await queryRunner.query(`DROP TABLE \`users\``);
 }
}
