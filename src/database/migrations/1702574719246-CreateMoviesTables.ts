import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateMoviesTables1702574719246 implements MigrationInterface {
 name = 'CreateMoviesTables1702574719246';

 public async up(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `CREATE TABLE \`actors\` (\`id\` varchar(36) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, \`name\` varchar(255) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
  );
  await queryRunner.query(
   `CREATE TABLE \`ratings\` (\`id\` varchar(36) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, \`userId\` varchar(36) NULL, \`movieId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
  );
  await queryRunner.query(
   `CREATE TABLE \`movies\` (\`id\` varchar(36) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, \`title\` varchar(255) NOT NULL, \`director\` varchar(255) NOT NULL, \`release_date\` varchar(255) NOT NULL, \`image\` text NULL, \`image_key\` varchar(255) NULL, \`synopsis\` varchar(255) NOT NULL, \`duration\` varchar(255) NULL, \`classification\` enum ('Livre', '10 anos', '12 anos', '14 anos', '16 anos', '18 anos') NULL, \`trailer\` varchar(255) NULL, \`status\` enum ('ativo', 'inativo') NOT NULL DEFAULT 'ativo', \`genreId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
  );
  await queryRunner.query(
   `CREATE TABLE \`genres\` (\`id\` varchar(36) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, \`name\` varchar(255) NOT NULL, \`description\` varchar(255) NULL, \`image\` text NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
  );
  await queryRunner.query(
   `CREATE TABLE \`movies_actors_actors\` (\`moviesId\` varchar(36) NOT NULL, \`actorsId\` varchar(36) NOT NULL, INDEX \`IDX_638b1d6f6929495fa5b87206da\` (\`moviesId\`), INDEX \`IDX_6f9bbef3136f7efc40a5a55886\` (\`actorsId\`), PRIMARY KEY (\`moviesId\`, \`actorsId\`)) ENGINE=InnoDB`,
  );
  await queryRunner.query(
   `ALTER TABLE \`ratings\` ADD CONSTRAINT \`FK_4d0b0e3a4c4af854d225154ba40\` FOREIGN KEY (\`userId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
  );
  await queryRunner.query(
   `ALTER TABLE \`ratings\` ADD CONSTRAINT \`FK_c10d219b6360c74a9f2186b76df\` FOREIGN KEY (\`movieId\`) REFERENCES \`movies\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
  );
  await queryRunner.query(
   `ALTER TABLE \`movies\` ADD CONSTRAINT \`FK_a58515bc480ff9004f5b4ebd40b\` FOREIGN KEY (\`genreId\`) REFERENCES \`genres\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
  );
  await queryRunner.query(
   `ALTER TABLE \`movies_actors_actors\` ADD CONSTRAINT \`FK_638b1d6f6929495fa5b87206daf\` FOREIGN KEY (\`moviesId\`) REFERENCES \`movies\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`,
  );
  await queryRunner.query(
   `ALTER TABLE \`movies_actors_actors\` ADD CONSTRAINT \`FK_6f9bbef3136f7efc40a5a55886c\` FOREIGN KEY (\`actorsId\`) REFERENCES \`actors\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
  );
 }

 public async down(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `ALTER TABLE \`movies_actors_actors\` DROP FOREIGN KEY \`FK_6f9bbef3136f7efc40a5a55886c\``,
  );
  await queryRunner.query(
   `ALTER TABLE \`movies_actors_actors\` DROP FOREIGN KEY \`FK_638b1d6f6929495fa5b87206daf\``,
  );
  await queryRunner.query(
   `ALTER TABLE \`movies\` DROP FOREIGN KEY \`FK_a58515bc480ff9004f5b4ebd40b\``,
  );
  await queryRunner.query(
   `ALTER TABLE \`ratings\` DROP FOREIGN KEY \`FK_c10d219b6360c74a9f2186b76df\``,
  );
  await queryRunner.query(
   `ALTER TABLE \`ratings\` DROP FOREIGN KEY \`FK_4d0b0e3a4c4af854d225154ba40\``,
  );
  await queryRunner.query(
   `DROP INDEX \`IDX_6f9bbef3136f7efc40a5a55886\` ON \`movies_actors_actors\``,
  );
  await queryRunner.query(
   `DROP INDEX \`IDX_638b1d6f6929495fa5b87206da\` ON \`movies_actors_actors\``,
  );
  await queryRunner.query(`DROP TABLE \`movies_actors_actors\``);
  await queryRunner.query(`DROP TABLE \`genres\``);
  await queryRunner.query(`DROP TABLE \`movies\``);
  await queryRunner.query(`DROP TABLE \`ratings\``);
  await queryRunner.query(`DROP TABLE \`actors\``);
 }
}
