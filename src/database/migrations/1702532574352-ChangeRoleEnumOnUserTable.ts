import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChagenRoleEnumOnUserTable1702532574352
 implements MigrationInterface
{
 name = 'ChagenRoleEnumOnUserTable1702532574352';

 public async up(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `ALTER TABLE \`users\` CHANGE \`role\` \`role\` enum ('ADMIN', 'USER', 'MASTER') NOT NULL DEFAULT 'USER'`,
  );
 }

 public async down(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `ALTER TABLE \`users\` CHANGE \`role\` \`role\` enum ('ADMIN', 'USER') NOT NULL DEFAULT 'USER'`,
  );
 }
}
