import { MigrationInterface, QueryRunner } from "typeorm";

export class AddRateCollumn1702586312127 implements MigrationInterface {
    name = 'AddRateCollumn1702586312127'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`ratings\` ADD \`rate\` int NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`actors\` CHANGE \`name\` \`name\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`title\` \`title\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`director\` \`director\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`release_date\` \`release_date\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`synopsis\` \`synopsis\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`duration\` \`duration\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`trailer\` \`trailer\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`genres\` CHANGE \`name\` \`name\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`genres\` CHANGE \`description\` \`description\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`genres\` CHANGE \`description\` \`description\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`genres\` CHANGE \`name\` \`name\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`trailer\` \`trailer\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`duration\` \`duration\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`synopsis\` \`synopsis\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`release_date\` \`release_date\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`director\` \`director\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`movies\` CHANGE \`title\` \`title\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`actors\` CHANGE \`name\` \`name\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`ratings\` DROP COLUMN \`rate\``);
    }

}
