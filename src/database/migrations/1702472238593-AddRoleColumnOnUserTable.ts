import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRoleColumnOnUserTable1702472238593
 implements MigrationInterface
{
 name = 'AddRoleColumnOnUserTable1702472238593';

 public async up(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(
   `ALTER TABLE \`users\` ADD \`role\` enum ('ADMIN', 'USER') NOT NULL DEFAULT 'USER'`,
  );
 }

 public async down(queryRunner: QueryRunner): Promise<void> {
  await queryRunner.query(`ALTER TABLE \`users\` DROP COLUMN \`role\``);
 }
}
