import genreSeed from './genre.seed';
import UserSeed from './user.seed';

export class Seeds {
 public async run(): Promise<void> {
  await UserSeed.run();
  await genreSeed.run(); 
 }
}

const seeds = new Seeds();

seeds
 .run()
 .then(() => {
  console.log('Seeds runned successfully');
  process.exit();
 })
 .catch(error => {
  console.error(error);
  process.exit(1);
 });
