import dataSource from '../data-source';

import Genre from '../../app/models/Genre.model';

class GenreCreate {
 public async run(): Promise<void> {
  console.log('Initializing seeder...');
  if (dataSource.isInitialized === false) await dataSource.initialize();
  const genresRepository = dataSource.getRepository(Genre);

  console.log('Creating genres...');

  const genres = await genresRepository.find();

  if (genres && genres.length > 0) {
   console.log('Genres already exist');
   return;
  }

  await genresRepository
   .createQueryBuilder()
   .insert()
   .into(Genre)
   .values([
    {
     name: 'Ação',
    },
    {
     name: 'Animação',
    },
    {
     name: 'Aventura',
    },
    {
     name: 'Comédia',
    },
    {
     name: 'Suspense',
    },
    {
     name: 'Terror',
    },
   ])
   .execute();
 }
}

export default new GenreCreate();
