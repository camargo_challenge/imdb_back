import dataSource from '../data-source';

import User from '../../app/models/User.model';

import PasswordHelper from '../../utils/helpers/password';

import { UserRoleEnum } from '../../utils/enums/user-role.enum';

class UserCreate {
 public async run(): Promise<void> {
  console.log('Initializing seeder...');
  if (dataSource.isInitialized === false) await dataSource.initialize();
  const userRepository = dataSource.getRepository(User);

  console.log('Creating users...');

  const users = await userRepository.find();

  if (users && users.length > 0) {
   console.log('Users already exist');
   return;
  }

  await userRepository
   .createQueryBuilder()
   .insert()
   .into(User)
   .values([
    {
     name: 'Admin Master',
     email: 'admin@imdb.com.br',
     password: PasswordHelper.hash('Adm@123_456'),
     role: UserRoleEnum.MASTER,
    },
   ])
   .execute();
 }
}

export default new UserCreate();
