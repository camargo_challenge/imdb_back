interface IDataPagination {
 data: any[];
 count: number;
}

class PaginationHelper {
 public paginate(dataObj: IDataPagination, limit: number, page: number) {
  const { data, count } = dataObj;
  const result = {
   data,
   total: count,
   pages: Math.ceil(count / limit),
   limit: limit,
   page: page,
  };

  return result;
 }
}

export default new PaginationHelper();
