import jwt from 'jsonwebtoken';

import { TokenPayload } from '@dtos/auth/auth-output.dto';

class JwtHelper {
 public createToken(payload: TokenPayload) {
  return jwt.sign(payload, process.env.JWT_SECRET as string, {
   expiresIn: '7d',
  });
 }

 public createRefreshToken(payload: TokenPayload) {
  return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET as string, {
   expiresIn: '30d',
  });
 }

 public verifyToken(token: string) {
  return jwt.verify(token, process.env.JWT_SECRET as string) as TokenPayload;
 }

 public verifyRefreshToken(token: string) {
  return jwt.verify(
   token,
   process.env.REFRESH_TOKEN_SECRET as string,
  ) as TokenPayload;
 }
}

export default new JwtHelper();
