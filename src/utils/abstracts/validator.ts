import { Request } from 'express';
import * as Yup from 'yup';

import { RequestProperyType } from '@customTypes/request-property';

import AppException from '@errors/app-exception';

abstract class BaseValidator {
 constructor() {}

 private parseJson = (value: any, mustBeArray = false) => {
  if (typeof value !== 'string') {
   return value;
  }

  try {
   if (mustBeArray && !value.startsWith('[')) {
    value = `[${value}]`;
   }

   const parsed = JSON.parse(value);

   return parsed;
  } catch (error) {
   return value;
  }
 };

 private yupValidation = async (schema: Yup.AnyObjectSchema, data: any) => {
  if (data?.actors && typeof data.actors === 'string') {
   data.actors = this.parseJson(data.actors, true);
  }

  if (data?.genre && typeof data.genre === 'string') {
   data.genre = this.parseJson(data.genre);
  }

  return await schema.validate(data, {
   abortEarly: false,
   stripUnknown: true,
   strict: false,
  });
 };

 protected async validateSchema(
  property: RequestProperyType,
  schema: Yup.AnyObjectSchema,
  req: Request,
 ) {
  try {
   req[property] = await this.yupValidation(schema, req[property]);

   return true;
  } catch (err: any) {
   throw new AppException(err.errors, 400);
  }
 }
}

export default BaseValidator;
