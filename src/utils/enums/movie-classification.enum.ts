export enum MovieClassificationEnum {
 FREE = 'Livre',
 TEN = '10 anos',
 TWELVE = '12 anos',
 FOURTEEN = '14 anos',
 SIXTEEN = '16 anos',
 EIGHTEEN = '18 anos',
}
