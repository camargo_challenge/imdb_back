import User from '@models/User.model';

import dataSource from '@database/data-source';

import { CreateAccountInput } from '@dtos/user/create-account.dto';
import { UpdateAccountInput } from '@dtos/user/update-account.dto';

import PasswordHelper from '@helpers/password';

import { UserStatusEnum } from '@enums/user-status.enum';

import { UserRoleEnum } from '@utils/enums/user-role.enum';

import AppException from '@errors/app-exception';
import ErrorMessages from '@errors/messages/index';
import { handleErrors } from '@errors/handle-errors';

class Service {
 private readonly repository;

 constructor() {
  this.repository = dataSource.getRepository(User);
 }

 public async create(user: CreateAccountInput): Promise<User> {
  try {
   const verifyEmail = await this.findByField('email', user.email, false);

   if (verifyEmail) {
    throw new AppException(ErrorMessages?.USER_ALREADY_EXISTS, 409);
   }

   const newUser = this.repository.create({
    ...user,
    password: PasswordHelper.hash(user.password),
   });

   return await this.repository.save(newUser);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findAll(
  page: number = 1,
  limit: number = 10,
  status?: UserStatusEnum,
  role?: UserRoleEnum,
  search?: string,
 ): Promise<{ data: User[]; count: number }> {
  try {
   const [data, count] = await this.repository.findAndCount({
    where: {
     ...(status && { status }),
     ...(search && { name: search }),
     ...(role && { role }),
    },
    skip: (page - 1) * limit,
    take: limit,
   });

   return { data, count };
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findByField(
  field: keyof User,
  value: string,
  mustExists = true,
  isAuth = false,
  select?: (keyof User)[],
 ): Promise<User | null> {
  try {
   const user = await this.repository.findOne({
    where: { [field]: value },
    ...(select && { select }),
   });

   if (!user && mustExists && !isAuth) {
    throw new AppException(ErrorMessages?.USER_NOT_FOUND, 404);
   }

   if (user && !mustExists) {
    throw new AppException(ErrorMessages?.USER_ALREADY_EXISTS, 409);
   }

   return user;
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async update(
  id: string,
  user: UpdateAccountInput,
 ): Promise<User> {
  try {
   const userExists = await this.findByField('id', id);

   if (!userExists) {
    throw new AppException(ErrorMessages?.USER_NOT_FOUND, 404);
   }

   if (user.email && user.email !== userExists.email)
    await this.validateEmailChange(user.email);

   if (user.password) user.password = PasswordHelper.hash(user.password);

   return await this.repository.save({
    ...userExists,
    ...user,
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async softDelete(id: string): Promise<User> {
  try {
   const userExists = await this.findByField('id', id);

   if (!userExists) {
    throw new AppException(ErrorMessages?.USER_NOT_FOUND, 404);
   }

   return await this.repository.save({
    ...userExists,
    status:
     userExists.status === UserStatusEnum.ACTIVE
      ? UserStatusEnum.INACTIVE
      : UserStatusEnum.ACTIVE,
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 private async validateEmailChange(email: string) {
  try {
   const verifyEmail = await this.findByField('email', email, false);

   if (verifyEmail) {
    throw new AppException(ErrorMessages?.USER_ALREADY_EXISTS, 409);
   }

   return true;
  } catch (error: any) {
   throw handleErrors(error);
  }
 }
}

export default new Service();
