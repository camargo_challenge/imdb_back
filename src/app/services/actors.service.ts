import dataSource from '@database/data-source';

import Actor from '@models/Actor.model';

import { handleErrors } from '@errors/handle-errors';
import { MovieStatusEnum } from '@utils/enums/movie-status.enum';

class Service {
 private readonly repository;

 constructor() {
  this.repository = dataSource.getRepository(Actor);
 }
 public async findAll(activeOnly = true): Promise<Actor[]> {
  try {
   return await this.repository.find({
    order: { name: 'ASC' },
    where: {
     ...(activeOnly && { movies: { status: MovieStatusEnum.ACTIVE } }),
    },
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }
}

export default new Service();
