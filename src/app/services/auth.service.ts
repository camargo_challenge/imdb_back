import JwtHelper from '@helpers/jwt';

import userService from './user.service';

import User from '@models/User.model';

import PasswordHelper from '@helpers/password';

import AppException from '@errors/app-exception';
import ErrorMessages from '@errors/messages/index';
import { handleErrors } from '@errors/handle-errors';

import { UserStatusEnum } from '@utils/enums/user-status.enum';

import { AuthInput } from '@dtos/auth/auth-input.dto';
import { AuthOutput } from '@dtos/auth/auth-output.dto';

class Service {
 constructor() {}

 public async login(user: AuthInput): Promise<AuthOutput> {
  try {
   const existingUser = await userService.findByField(
    'email',
    user.email,
    true,
    true,
    [
     'id',
     'name',
     'email',
     'password',
     'role',
     'status',
     'createdAt',
     'deletedAt',
     'updatedAt',
     'image',
     'imageKey',
    ],
   );

   if (!existingUser) {
    throw new AppException(ErrorMessages?.INVALID_CREDENTIALS, 404);
   }

   this.verifyPassword(user.password, existingUser.password);

   if (existingUser.status !== UserStatusEnum.ACTIVE) {
    throw new AppException(ErrorMessages?.USER_INACTIVE, 403);
   }

   return this.generateAccess(existingUser);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async refresh(refreshToken: string): Promise<AuthOutput> {
  try {
   const payload = JwtHelper.verifyRefreshToken(refreshToken);
   const user = await userService.findByField('id', payload.id, true, true);

   if (!user) {
    throw new AppException(ErrorMessages?.INVALID_CREDENTIALS, 404);
   }

   return this.generateAccess(user);
  } catch (error: any) {
   throw handleErrors(error, ErrorMessages?.UNATHORIZED);
  }
 }

 private verifyPassword(password: string, hash: string) {
  const passwordMatch = PasswordHelper.comparePasswordAndHash(password, hash);

  if (!passwordMatch) {
   throw new AppException(ErrorMessages?.INVALID_CREDENTIALS, 400);
  }
 }

 private generateAccess(user: User) {
  const token = JwtHelper.createToken({
   id: user.id,
   role: user.role,
   name: user.name,
  });

  const refreshToken = JwtHelper.createRefreshToken({
   id: user.id,
   role: user.role,
   name: user.name,
  });

  return {
   user: {
    id: user.id,
    name: user.name,
    email: user.email,
    role: user.role,
    status: user.status,
    image: user.image,
    imageKey: user.imageKey,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
    deletedAt: user.deletedAt,
   },
   token,
   refreshToken,
  };
 }
}

export default new Service();
