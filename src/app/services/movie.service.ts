import { Brackets } from 'typeorm';

import Movie from '@models/Movie.model';

import dataSource from '@database/data-source';

import GenreService from '@services/genre.service';
import UserService from '@services/user.service';

import { CreateMovieInput } from '@dtos/movie/create-movie.dto';
import { UpdateMovieInput } from '@dtos/movie/update-movie.dto';

import { MovieStatusEnum } from '@utils/enums/movie-status.enum';

import AppException from '@errors/app-exception';
import ErrorMessages from '@errors/messages/index';
import { handleErrors } from '@errors/handle-errors';
import Rating from '@models/Rating.model';

class Service {
 private readonly repository;

 constructor() {
  this.repository = dataSource.getRepository(Movie);
 }

 public async create(movie: CreateMovieInput): Promise<Movie> {
  try {
   const genre = await GenreService.findById(movie?.genre?.id);

   if (!genre) throw new AppException(ErrorMessages?.GENRE_NOT_FOUND, 404);

   const newMovie = this.repository.create({
    ...movie,
    releaseDate: String(movie.releaseDate),
   });

   return await this.repository.save(newMovie);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findAll(
  page: number | undefined = 1,
  limit: number | undefined = 10,
  actors?: string[],
  directors?: string[],
  genres?: string[],
  search?: string,
 ): Promise<{ data: Movie[]; count: number }> {
  try {
   const whereClause = this.buildWhereClause(actors, directors, genres, search);

   const [data, count] = await this.repository
    .createQueryBuilder('movies')
    .leftJoinAndSelect('movies.actors', 'actors')
    .leftJoinAndSelect('movies.genre', 'genre')
    .where(whereClause)
    .andWhere('movies.status = :status', { status: MovieStatusEnum.ACTIVE })
    .getManyAndCount();

   return { data, count };
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findByField(
  field: keyof Movie,
  value: string,
  select?: (keyof Movie)[],
 ): Promise<Movie & { avarageRate: number }> {
  try {
   const movie = await this.repository.findOne({
    where: { [field]: value },
    ...(select && { select }),
    relations: ['actors', 'genre', 'ratings', 'ratings.user'],
   });

   if (!movie) {
    throw new AppException(ErrorMessages?.MOVIE_NOT_FOUND, 404);
   }

   const avarageRate =
    movie.ratings.reduce((acc, rating) => acc + rating?.rate, 0) /
    movie.ratings.length;

   return {
    ...movie,
    avarageRate,
   };
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findAllDirectors(): Promise<string[]> {
  try {
   const directors = await this.repository
    .createQueryBuilder('movies')
    .select('movies.director')
    .distinct(true)
    .getRawMany();

   return directors.map(director => director.movies_director);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async update(id: string, movie: UpdateMovieInput): Promise<Movie> {
  try {
   const movieExists = await this.findByField('id', id);
   const hasActorDeleted = movie?.actors?.some(actor => actor?.wasDeleted);

   if (!movieExists) {
    throw new AppException(ErrorMessages?.MOVIE_NOT_FOUND, 404);
   }

   if (movie.genre && movie.genre.id) {
    const genre = await GenreService.findById(movie?.genre?.id);

    if (!genre) throw new AppException(ErrorMessages?.GENRE_NOT_FOUND, 404);
   }

   if (hasActorDeleted) {
    const deletedActors = movie?.actors?.filter(actor => actor?.wasDeleted);

    movie.actors = movie?.actors?.filter(actor => !actor?.wasDeleted);
    movieExists.actors = movieExists.actors.filter(
     actor =>
      !deletedActors?.some(deletedActor => deletedActor.id === actor.id),
    );
   }

   return await this.repository.save({
    ...movieExists,
    ...movie,
    trailer: movie?.trailer ? movie?.trailer : undefined,
    releaseDate: String(movie.releaseDate),
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async softDelete(id: string): Promise<Movie> {
  try {
   const userExists = await this.findByField('id', id);

   if (!userExists) {
    throw new AppException(ErrorMessages?.MOVIE_NOT_FOUND, 404);
   }

   return await this.repository.save({
    ...userExists,
    status:
     userExists.status === MovieStatusEnum.ACTIVE
      ? MovieStatusEnum.INACTIVE
      : MovieStatusEnum.ACTIVE,
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async rate(id: string, userId: string, rate: number): Promise<Movie> {
  try {
   const movie = await this.findByField('id', id);
   const user = await UserService.findByField('id', userId);

   if (!movie) {
    throw new AppException(ErrorMessages?.MOVIE_NOT_FOUND, 404);
   }

   if (!user) {
    throw new AppException(ErrorMessages?.USER_NOT_FOUND, 404);
   }

   const userRate = movie.ratings.find(rating => rating?.user?.id === userId);

   if (userRate) {
    userRate.rate = rate;
   } else {
    const newRate = dataSource
     .getRepository(Rating)
     .create({ user, rate, movie });
    movie.ratings.push(newRate);
   }

   return await this.repository.save(movie);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 private buildWhereClause(
  actors?: string[] | string,
  directors?: string[] | string,
  genres?: string[] | string,
  search?: string,
 ) {
  return new Brackets(qb => {
   if (actors?.length && actors?.length > 0) {
    if (Array.isArray(actors) && actors.length > 0) {
     qb.andWhere('actors.id IN (:...actors) ', { actors });
    } else {
     qb.andWhere('actors.id LIKE :actors', {
      actors: `%${actors?.toString().replace('[', '').replace(']', '')}%`,
     });
    }
   }

   if (directors?.length && directors?.length > 0) {
    if (Array.isArray(directors) && directors.length > 0) {
     qb.andWhere('movies.director IN (:...directors)', { directors });
    } else {
     qb.andWhere('movies.director LIKE :directors', {
      directors: `%${directors?.toString().replace('[', '').replace(']', '')}%`,
     });
    }
   }

   if (genres?.length && genres?.length > 0) {
    if (Array.isArray(genres) && genres.length > 0) {
     qb.andWhere('genre.id IN (:...genres)', { genres });
    } else {
     qb.andWhere('genre.id LIKE :genres', {
      genres: `%${genres?.toString().replace('[', '').replace(']', '')}%`,
     });
    }
   }

   if (search) {
    qb.andWhere('movies.title LIKE :search', { search: `%${search}%` });
   }
   return qb;
  });
 }
}

export default new Service();
