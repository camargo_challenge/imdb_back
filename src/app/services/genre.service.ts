import dataSource from '@database/data-source';

import Genre from '@models/Genre.model';

import ErrorMessages from '@errors/messages/index';
import AppException from '@errors/app-exception';
import { handleErrors } from '@errors/handle-errors';
import { MovieStatusEnum } from '@utils/enums/movie-status.enum';

class Service {
 private readonly repository;

 constructor() {
  this.repository = dataSource.getRepository(Genre);
 }

 public async create(name: string): Promise<Genre> {
  try {
   const genre = this.repository.create({ name });

   return await this.repository.save(genre);
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findAll(activeOnly = true): Promise<Genre[]> {
  try {
   return await this.repository.find({
    order: { name: 'ASC' },
    relations: ['movies'],
    where: {
     ...(activeOnly && { movies: { status: MovieStatusEnum.ACTIVE } }),
    },
   });
  } catch (error: any) {
   throw handleErrors(error);
  }
 }

 public async findById(id: string): Promise<Genre> {
  try {
   const genre = await this.repository.findOne({
    where: { id },
    relations: ['movies'],
   });

   if (!genre) throw new AppException(ErrorMessages?.GENRE_NOT_FOUND, 404);

   return genre;
  } catch (error: any) {
   throw handleErrors(error);
  }
 }
}

export default new Service();
