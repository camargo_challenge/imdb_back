import { RequestHandler, Request, Response, NextFunction } from 'express';
import * as Yup from 'yup';

import { RequestProperyType } from '@utils/types/request-property';
import BaseValidator from '@utils/abstracts/validator';

class SchemaValidationMiddleware extends BaseValidator {
 public validate(
  schema: Yup.AnyObjectSchema,
  property: RequestProperyType,
 ): RequestHandler {
  return async (req: Request, res: Response, next: NextFunction) => {
   try {
    await this.validateSchema(property, schema, req);
    next();
   } catch (err: any) {
    return res.status(404).json({
     error: `Erro na validação: ${err.message}`,
    });
   }
  };
 }
}

export default new SchemaValidationMiddleware();
