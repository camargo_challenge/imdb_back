import { RequestHandler, Request, Response, NextFunction } from 'express';

import Passport from '@config/passport';

import { UserRoleEnum } from '@utils/enums/user-role.enum';

import UserService from '@services/user.service';

import AppException from '@errors/app-exception';
import ErrorMessages from '@errors/messages/index';
import { UserStatusEnum } from '@utils/enums/user-status.enum';

class AuthMiddleware {
 public isAuthenticated: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
 ) => {
  try {
   Passport.authenticate(
    'jwt',
    { session: false, failWithError: true },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (err: any, payload: any, info: any) => {
     if (err) return next(err);
     if (!payload)
      return next(new AppException(ErrorMessages.UNATHORIZED, 401));
     else req.auth = payload;
    },
   )(req, res, next);

   const user = await UserService.findByField('id', req?.auth?.id, true, true);

   if (!user) return next(new AppException(ErrorMessages.UNATHORIZED, 401));

   if (user.status !== UserStatusEnum.ACTIVE)
    return next(new AppException(ErrorMessages.UNATHORIZED, 401));

   // Atualiza o objeto auth com as informações mais recentes do usuário e deixa de utilizar as informações que estavam armazenadas no token.
   req.auth.role = user.role;
   req.auth.status = user.status;

   next();
  } catch (err: any) {
   next(new AppException(ErrorMessages.UNATHORIZED, 401));
  }
 };

 public isAdmin: RequestHandler = (req, res, next) => {
  try {
   if (
    req.auth.role !== UserRoleEnum.ADMIN &&
    req.auth.role !== UserRoleEnum.MASTER
   )
    throw new Error();

   next();
  } catch (err: any) {
   next(new AppException(ErrorMessages.FORBIDDEN, 403));
  }
 };

 public isUser: RequestHandler = (req, res, next) => {
  try {
   if (req.auth.role !== UserRoleEnum.USER) throw new Error();

   next();
  } catch (err: any) {
   next(new AppException(ErrorMessages.FORBIDDEN, 403));
  }
 };
}

export default new AuthMiddleware();
