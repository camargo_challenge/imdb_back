import {
 Column,
 Entity,
 JoinTable,
 ManyToMany,
 ManyToOne,
 OneToMany,
} from 'typeorm';

import { BaseEntity } from '.';
import Genre from './Genre.model';

import { MovieClassificationEnum } from '../../utils/enums/movie-classification.enum';
import Actor from './Actor.model';
import Rating from './Rating.model';
import { MovieStatusEnum } from '../../utils/enums/movie-status.enum';

@Entity('movies')
export default class Movie extends BaseEntity {
 @Column({ type: 'varchar', nullable: false, width: 255 })
 title: string;

 @Column({ type: 'varchar', nullable: false, width: 255 })
 director: string;

 @Column({ name: 'release_date', type: 'varchar', nullable: false, width: 255 })
 releaseDate: string;

 @Column({ type: 'text', nullable: true })
 image: string;

 @Column({ name: 'image_key', type: 'varchar', nullable: true })
 imageKey: string;

 @Column({ type: 'varchar', nullable: false, width: 500 })
 synopsis: string;

 @Column({ type: 'varchar', nullable: true, width: 255 })
 duration: string;

 @Column({ type: 'enum', enum: MovieClassificationEnum, nullable: true })
 classification: string;

 @Column({ type: 'varchar', nullable: true, width: 255 })
 trailer: string;

 @ManyToOne(() => Genre, genre => genre.movies)
 genre: Genre;

 @ManyToMany(() => Actor, actor => actor.movies, {
  cascade: ['insert', 'update', 'remove'],
 })
 @JoinTable()
 actors: Actor[];

 @OneToMany(() => Rating, rating => rating.movie, {
  cascade: ['insert', 'update', 'remove'],
 })
 ratings: Rating[];

 @Column({
  type: 'enum',
  enum: MovieStatusEnum,
  default: MovieStatusEnum.ACTIVE,
  nullable: false,
 })
 status: MovieStatusEnum;
}
