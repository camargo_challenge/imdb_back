import { Column, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from '.';

import Movie from './Movie.model';

@Entity('genres')
export default class Genre extends BaseEntity {
 @Column({ type: 'varchar', nullable: false, width: 255 })
 name: string;

 @Column({ type: 'varchar', nullable: true, width: 255 })
 description: string;

 @Column({ type: 'text', nullable: true })
 image: string;

 @OneToMany(() => Movie, movie => movie.genre)
 movies: Movie[];
}
