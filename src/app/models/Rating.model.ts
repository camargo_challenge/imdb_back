import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '.';

import User from './User.model';
import Movie from './Movie.model';

@Entity('ratings')
export default class Rating extends BaseEntity {
 @ManyToOne(() => User, user => user.ratings)
 user: User;

 @ManyToOne(() => Movie, movie => movie.ratings)
 movie: Movie;

 @Column({ type: 'int', nullable: false })
 rate: number;
}
