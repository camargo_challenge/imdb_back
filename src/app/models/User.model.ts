import { Column, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from '.';
import Rating from './Rating.model';

import { UserStatusEnum } from '../../utils/enums/user-status.enum';
import { UserRoleEnum } from '../../utils/enums/user-role.enum';

@Entity('users')
export default class User extends BaseEntity {
 @Column({ type: 'varchar', nullable: true })
 name: string;

 @Column({ type: 'varchar', unique: true, nullable: false })
 email: string;

 @Column({ type: 'varchar', nullable: true })
 image: string;

 @Column({ name: 'image_key', type: 'varchar', nullable: true })
 imageKey: string;

 @Column({ type: 'varchar', nullable: true, select: false })
 password: string;

 @Column({
  type: 'enum',
  enum: UserStatusEnum,
  default: UserStatusEnum.ACTIVE,
  nullable: false,
 })
 status: UserStatusEnum;

 @Column({
  type: 'enum',
  enum: UserRoleEnum,
  default: UserRoleEnum.USER,
  nullable: false,
 })
 role: UserRoleEnum;

 @OneToMany(() => Rating, rating => rating.user)
 ratings: Rating[];
}
