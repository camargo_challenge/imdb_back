import { Column, Entity, ManyToMany } from 'typeorm';

import { BaseEntity } from '.';
import Movie from './Movie.model';

@Entity('actors')
export default class Actor extends BaseEntity {
 @Column({ type: 'varchar', nullable: false, width: 255 })
 name: string;

 @ManyToMany(() => Movie, movie => movie.actors)
 movies: Movie[];
}
