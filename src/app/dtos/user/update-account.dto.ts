import * as Yup from 'yup';

import { UserRoleEnum } from '@utils/enums/user-role.enum';

export type UpdateAccountInput = Yup.InferType<typeof UpdateAccountDto>;

export const UpdateAccountDto = Yup.object().shape(
 {
  name: Yup.string()
   .max(255, 'Nome deve ter no máximo ${max} caracteres')
   .trim()
   .transform(value => (value ? value : undefined))
   .when('name', {
    is: (value: any) => value?.length,
    then: rule => rule.min(3),
   })
   .max(255, 'Nome deve ter no máximo ${max} caracteres')
   .label('Nome')
   .optional(),
  email: Yup.string()
   .email('Email inválido')
   .label('Email')
   .transform(value => (value ? value : undefined))
   .optional(),
  password: Yup.string()
   .transform(value => (value ? value : undefined))
   .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,255})/, {
    message:
     'Senha deve ter no mínimo 8 caracteres, uma letra maiúscula, uma minúscula, um número e um caractere especial',
    excludeEmptyString: true,
   })
   .label('Senha')
   .optional(),
  passwordConfirmation: Yup.string()
   .transform(value => (value ? value : undefined))
   .oneOf([Yup.ref('password')], 'Senhas devem ser iguais')
   .label('Confirmação de senha')
   .when('password', {
    is: (value: any) => value?.length,
    then: rule => rule.min(8).required(),
   })
   .optional(),
  role: Yup.string()
   .transform(value => (value ? value : undefined))
   .oneOf(Object.values(UserRoleEnum))
   .label('Role')
   .optional(),
  image: Yup.string()
   .url()
   .transform(value => (value === '' ? undefined : value))
   .label('Imagem')
   .optional(),
 },
 [['name', 'name']],
);
