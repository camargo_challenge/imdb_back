import * as Yup from 'yup';

import { UserRoleEnum } from '@utils/enums/user-role.enum';

export type CreateAccountInput = Yup.InferType<typeof CreateAccountDto>;

export const CreateAccountDto = Yup.object().shape({
 name: Yup.string()
  .trim()
  .min(3, 'Nome deve ter no mínimo ${min} caracteres')
  .max(255, 'Nome deve ter no máximo ${max} caracteres')
  .label('Nome')
  .required(),
 email: Yup.string().email('Email inválido').label('Email').required(),
 password: Yup.string()
  .matches(
   /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,255})/,
   'Senha deve ter no mínimo 8 caracteres, uma letra maiúscula, uma minúscula, um número e um caractere especial',
  )
  .label('Senha')
  .required(),
 passwordConfirmation: Yup.string()
  .oneOf([Yup.ref('password')], 'Senhas devem ser iguais')
  .label('Confirmação de senha')
  .min(8, 'Confirmação de Senha deve ter no mínimo ${min} caracteres')
  .required(),
 role: Yup.string().oneOf(Object.values(UserRoleEnum)).label('Role').required(),
 image: Yup.string()
  .url()
  .transform(value => (value === '' ? undefined : value))
  .label('Imagem')
  .optional(),
});
