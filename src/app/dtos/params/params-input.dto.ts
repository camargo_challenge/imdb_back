import * as Yup from 'yup';

export type ParamsInput = Yup.InferType<typeof ParamsSchema>;

export const ParamsSchema = Yup.object().shape({
 id: Yup.string().uuid().required(),
});
