import * as Yup from 'yup';

export type AuthInput = Yup.InferType<typeof AuthDto>;

export const AuthDto = Yup.object().shape({
 email: Yup.string().email('Email inválido').label('Email').required(),
 password: Yup.string()
  .min(8, 'Senha deve ter no mínimo ${min} caracteres')
  .max(255, 'Senha deve ter no máximo ${max} caracteres')
  .label('Senha')
  .required(),
});
