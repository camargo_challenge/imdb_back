import * as Yup from 'yup';

export type RefreshInput = Yup.InferType<typeof RefreshDto>;

export const RefreshDto = Yup.object().shape({
 refreshToken: Yup.string().label('Refresh Token').required(),
});
