import User from '@models/User.model';

export type AuthOutput = {
 user: Omit<User, 'password' | 'ratings' >;
 token: string;
 refreshToken: string;
};

export type TokenPayload = {
 id: string;
 role: string;
 name: string;
};

export type TokenPayloadRefresh = {
 id: string;
};
