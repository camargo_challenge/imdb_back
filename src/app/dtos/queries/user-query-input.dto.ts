import * as Yup from 'yup';

import { UserRoleEnum } from '@enums/user-role.enum';
import { UserStatusEnum } from '@enums/user-status.enum';

export type QueryInput = Yup.InferType<typeof QuerySchema>;

export const QuerySchema = Yup.object().shape({
 limit: Yup.number().positive().integer().optional(),
 page: Yup.number().positive().integer().optional(),
 status: Yup.string().oneOf(Object.values(UserStatusEnum)).optional(),
 role: Yup.string().oneOf(Object.values(UserRoleEnum)).optional(),
 search: Yup.string().trim().min(3).max(255).optional(),
});
