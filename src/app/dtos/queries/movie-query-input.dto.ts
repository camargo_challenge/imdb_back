import * as Yup from 'yup';

import { MovieClassificationEnum } from '@utils/enums/movie-classification.enum';
import { MovieStatusEnum } from '@utils/enums/movie-status.enum';

const stringOrArraySchema = Yup.lazy(value => {
 if (Array.isArray(value)) {
  return Yup.array().of(Yup.string().trim().min(3).max(255)).optional();
 } else {
  return Yup.string().trim().min(3).max(255).optional();
 }
});

export type QueryMovieInput = Yup.InferType<typeof QueryMovieSchema>;

export const QueryMovieSchema = Yup.object().shape({
 limit: Yup.number().positive().integer().optional(),
 page: Yup.number().positive().integer().optional(),
 status: Yup.string().oneOf(Object.values(MovieStatusEnum)).optional(),
 classification: Yup.string()
  .oneOf(Object.values(MovieClassificationEnum))
  .optional(),
 directors: stringOrArraySchema,
 actors: stringOrArraySchema,
 genres: stringOrArraySchema,
 search: Yup.string().trim().min(3).max(255).optional(),
 onlyWithMovies: Yup.boolean().optional(),
});
