import { MovieClassificationEnum } from '@utils/enums/movie-classification.enum';
import * as Yup from 'yup';

export type CreateMovieInput = Yup.InferType<typeof CreateMovieDto>;

export const CreateMovieDto = Yup.object().shape(
 {
  title: Yup.string()
   .trim()
   .min(3, 'Título deve ter no mínimo ${min} caracteres')
   .max(255, 'Título deve ter no máximo ${max} caracteres')
   .label('Título')
   .required(),
  director: Yup.string()
   .trim()
   .min(3, 'Diretor deve ter no mínimo ${min} caracteres')
   .max(255, 'Diretor deve ter no máximo ${max} caracteres')
   .label('Diretor')
   .required(),
  releaseDate: Yup.date().label('Data de lançamento').required(),
  synopsis: Yup.string()
   .trim()
   .min(3, 'Sinopse deve ter no mínimo ${min} caracteres')
   .max(500, 'Sinopse deve ter no máximo ${max} caracteres')
   .label('Sinopse')
   .required(),
  duration: Yup.string()
   .trim()
   .when('duration', {
    is: (duration: string) => duration?.length > 0,
    then: rule =>
     // verifica se tem só numero
     rule.matches(/^[0-9]*$/, 'Duração deve ser um número'),
   })
   .label('Duração')
   .optional(),
  classification: Yup.string()
   .trim()
   .oneOf(Object.values(MovieClassificationEnum))
   .label('Classificação')
   .optional(),
  genre: Yup.object().shape({
   id: Yup.string().uuid().label('Gênero').required(),
  }),
  actors: Yup.array().of(
   Yup.object().shape({
    id: Yup.string().uuid().label('Ator').optional(),
    name: Yup.string().label('Nome').optional(),
   }),
  ),
  trailer: Yup.string()
   .trim()
   .when('trailer', {
    is: (trailer: string) => trailer?.length > 0,
    then: rule =>
     rule.matches(
      /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/,
      'Trailer deve ser uma URL válida do Youtube',
     ),
   })
   .label('Trailer'),
  image: Yup.string()
   .url()
   .transform(value => (value === '' ? undefined : value))
   .label('Imagem')
   .optional(),
 },
 [
  ['trailer', 'trailer'],
  ['duration', 'duration'],
 ],
);
