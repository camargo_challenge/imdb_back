import { MovieClassificationEnum } from '@utils/enums/movie-classification.enum';
import * as Yup from 'yup';

export type UpdateMovieInput = Yup.InferType<typeof UpdateMovieDto>;

export const UpdateMovieDto = Yup.object().shape(
 {
  title: Yup.string()
   .trim()

   .label('Título')
   .transform(value => (value === '' ? undefined : value))
   .when('title', {
    is: (title: string) => title?.length > 0,
    then: rule =>
     rule
      .min(3, 'Título deve ter no mínimo ${min} caracteres')
      .max(255, 'Título deve ter no máximo ${max} caracteres'),
   })
   .optional(),
  director: Yup.string()
   .trim()
   .transform(value => (value === '' ? undefined : value))
   .label('Diretor')
   .when('director', {
    is: (director: string) => director?.length > 0,
    then: rule =>
     rule
      .min(3, 'Diretor deve ter no mínimo ${min} caracteres')
      .max(255, 'Diretor deve ter no máximo ${max} caracteres'),
   })
   .optional(),

  releaseDate: Yup.string()
   .transform(value => (value === '' ? undefined : value))
   .when('releaseDate', {
    is: (releaseDate: string) => releaseDate?.length > 0,
    then: rule => rule.matches(/^\d{4}-\d{2}-\d{2}$/, 'Data inválida'),
   })
   .optional(),
  synopsis: Yup.string()
   .trim()
   .label('Sinopse')
   .transform(value => (value === '' ? undefined : value))
   .when('synopsis', {
    is: (synopsis: string) => synopsis?.length > 0,
    then: rule =>
     rule
      .min(3, 'Sinopse deve ter no mínimo ${min} caracteres')
      .max(500, 'Sinopse deve ter no máximo ${max} caracteres'),
   })
   .optional(),
  duration: Yup.string()
   .trim()
   .transform(value => (value === '' ? undefined : value))
   .when('duration', {
    is: (duration: string) => duration?.length > 0,
    then: rule => rule.matches(/^[0-9]*$/, 'Duração deve ser um número'),
   })
   .label('Duração')
   .optional(),
  classification: Yup.string()
   .trim()
   .transform(value => (value === '' ? undefined : value))
   .oneOf(Object.values(MovieClassificationEnum))
   .label('Classificação')
   .optional(),
  genre: Yup.object()
   .transform(value => (value === '' ? undefined : value))
   .shape({
    id: Yup.string().uuid().label('Gênero').optional(),
   })
   .label('Gênero')
   .optional(),

  actors: Yup.array()
   .transform(value => (value === '' ? undefined : value))

   .of(
    Yup.object().shape({
     id: Yup.string().uuid().label('Ator').optional(),
     name: Yup.string().label('Nome').optional(),
     wasDeleted: Yup.boolean().label('Atores').optional(),
    }),
   ),
  trailer: Yup.string()
   .trim()
   .transform(value => (value === '' ? undefined : value))
   .nullable()
   .when('trailer', {
    is: (trailer: string) => trailer?.length > 0,
    then: rule =>
     rule.matches(
      /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/,
      'Trailer deve ser uma URL válida do Youtube',
     ),
   })
   .label('Trailer'),
  image: Yup.string()
   .url()
   .transform(value => (value === '' ? undefined : value))
   .label('Imagem')
   .optional(),
 },
 [
  ['trailer', 'trailer'],
  ['duration', 'duration'],
  ['title', 'title'],
  ['director', 'director'],
  ['releaseDate', 'releaseDate'],
  ['synopsis', 'synopsis'],
  ['classification', 'classification'],
  ['genre', 'genre'],
  ['actors', 'actors'],
 ],
);
