import * as Yup from 'yup';

export type RateMovieInput = Yup.InferType<typeof RateMovieDto>;

export const RateMovieDto = Yup.object().shape({
 rate: Yup.number()
  .min(1, 'Avaliação deve ser no mínimo ${min}')
  .max(4, 'Avaliação deve ser no máximo ${max}')
  .label('Avaliação')
  .required(),
});
