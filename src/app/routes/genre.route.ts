import { Router } from 'express';

import Controller from '@controllers/genre.controller';

import Validation from '@middlewares/schema-validation.middleware';
import { QueryMovieSchema } from '@dtos/queries/movie-query-input.dto';
import { ParamsSchema } from '@dtos/params/params-input.dto';

const genre = Router();

genre.get(
 '/',
 Validation.validate(QueryMovieSchema, 'query'),
 Controller.findAll,
);

genre.get(
 '/:id',
 Validation.validate(ParamsSchema, 'params'),
 Controller.findById,
);
export default genre;
