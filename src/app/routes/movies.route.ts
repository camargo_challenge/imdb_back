import { Router } from 'express';

import Auth from '@middlewares/auth.middleware';
import Validation from '@middlewares/schema-validation.middleware';

import Controller from '@controllers/movie.controller';

import { QueryMovieSchema } from '@dtos/queries/movie-query-input.dto';
import { CreateMovieDto } from '@dtos/movie/create-movie.dto';
import { ParamsSchema } from '@dtos/params/params-input.dto';
import { UpdateMovieDto } from '@dtos/movie/update-movie.dto';

const movies = Router();

movies
 .get('/', Validation.validate(QueryMovieSchema, 'query'), Controller.findAll)
 .get('/directors', Controller.findAllDirectors)
 .post(
  '/',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(CreateMovieDto, 'body'),
  Controller.create,
 )
 .get('/:id', Validation.validate(ParamsSchema, 'params'), Controller.findById)
 .patch(
  '/:id',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(ParamsSchema, 'params'),
  Validation.validate(UpdateMovieDto, 'body'),
  Controller.update,
 )
 .delete(
  '/:id',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(ParamsSchema, 'params'),
  Controller.softDelete,
 )
 .post(
  '/:id/rate',
  Auth.isAuthenticated,
  Auth.isUser,
  Validation.validate(ParamsSchema, 'params'),
  Controller.rate,
 );

export default movies;
