import { Router } from 'express';

import Controller from '@controllers/actors.controller';

const actorsRoute = Router();

actorsRoute.get('/', Controller.findAll);

export default actorsRoute;
