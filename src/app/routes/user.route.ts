import { Router } from 'express';

import Validation from '@middlewares/schema-validation.middleware';
import Auth from '@middlewares/auth.middleware';

import UserController from '@controllers/user.controller';

import { CreateAccountDto } from '@dtos/user/create-account.dto';
import { QuerySchema } from '@dtos/queries/user-query-input.dto';
import { UpdateAccountDto } from '@dtos/user/update-account.dto';
import { ParamsSchema } from '@dtos/params/params-input.dto';

const userRoute = Router();

userRoute
 .post(
  '/',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(CreateAccountDto, 'body'),
  UserController.create,
 )
 .get(
  '/',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(QuerySchema, 'query'),
  UserController.findAll,
 )
 .get(
  '/:id',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(ParamsSchema, 'params'),
  UserController.findById,
 )
 .patch(
  '/:id',
  Auth.isAuthenticated,
  Auth.isAdmin,
  Validation.validate(ParamsSchema, 'params'),
  Validation.validate(UpdateAccountDto, 'body'),
  UserController.update,
 )
 .delete(
  '/:id',
  Validation.validate(ParamsSchema, 'params'),
  UserController.softDelete,
 );

export default userRoute;
