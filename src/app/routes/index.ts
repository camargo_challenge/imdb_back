import { Router } from 'express';

import userRoute from './user.route';
import authRoute from './auth.route';
import myselfRoute from './myself.route';
import movieRoute from './movies.route';
import genreRoute from './genre.route';
import actorRoute from './actors.route';
import uploadRoute from './upload.route';

const routes = Router();

routes.use('/users', userRoute);

routes.use('/myself', myselfRoute);

routes.use('/movies', movieRoute);

routes.use('/auth', authRoute);

routes.use('/genres', genreRoute);

routes.use('/actors', actorRoute);

routes.use('/upload-file', uploadRoute);

routes.get('/', (req, res) => {
 res.json({ message: 'Hello World' });
});

export default routes;
