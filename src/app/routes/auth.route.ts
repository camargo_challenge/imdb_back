import { Router } from 'express';

import Validation from '@middlewares/schema-validation.middleware';

import Controller from '@controllers/auth.controller';
import { AuthDto } from '@dtos/auth/auth-input.dto';
import { RefreshDto } from '@dtos/auth/refresh-input.dto';

const authRoute = Router();

authRoute
 .post('/login', Validation.validate(AuthDto, 'body'), Controller.login)
 .post(
  '/refresh-token',
  Validation.validate(RefreshDto, 'body'),
  Controller.refresh,
 );

export default authRoute;
