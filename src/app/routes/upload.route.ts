import { Router } from 'express';
import multer from 'multer';

import Auth from '@middlewares/auth.middleware';

import multerOptions from '@config/storage';

const uploadRouter = Router();

uploadRouter.post(
 '/',
 multer(multerOptions).single('file'),
 Auth.isAuthenticated,
 Auth.isAdmin,
 (req, res) => {
  res.status(200).json({ file: req.file });
 },
);

export default uploadRouter;
