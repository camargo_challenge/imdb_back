import { Router } from 'express';
import multer from 'multer';

import Validation from '@middlewares/schema-validation.middleware';
import Auth from '@middlewares/auth.middleware';

import Controller from '@controllers/myself.controller';

import multerOptions from '@config/storage';

import { QuerySchema } from '@dtos/queries/user-query-input.dto';
import { UpdateAccountDto } from '@dtos/user/update-account.dto';

const myselfRoute = Router();

myselfRoute
 .get(
  '/',
  Auth.isAuthenticated,
  Validation.validate(QuerySchema, 'query'),
  Controller.findById,
 )
 .patch(
  '/',
  multer(multerOptions).single('image'),
  Auth.isAuthenticated,
  Validation.validate(UpdateAccountDto, 'body'),
  Controller.update,
 )
 .delete('/', Auth.isAuthenticated, Controller.softDelete);

export default myselfRoute;
