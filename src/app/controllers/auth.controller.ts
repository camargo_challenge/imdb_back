import { Request, Response } from 'express';

import Service from '@services/auth.service';

import { handleErrors } from '@errors/handle-errors';
import { AuthInput } from '@dtos/auth/auth-input.dto';

class Controller {
 public async login(req: Request, res: Response) {
  try {
   const { email, password } = req.body as AuthInput;
   const user = await Service.login({ email, password });
   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async refresh(req: Request, res: Response) {
  try {
   const { refreshToken } = req.body;
   const user = await Service.refresh(refreshToken);
   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
