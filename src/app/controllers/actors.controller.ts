import { Request, Response } from 'express';

import Service from '@services/actors.service';

import { handleErrors } from '@errors/handle-errors';

class Controller {
 public async findAll(req: Request, res: Response) {
  try {
   const actors = await Service.findAll();

   return res.status(200).json(actors);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
