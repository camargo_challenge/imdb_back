import { Request, Response } from 'express';

import Service from '@services/movie.service';

import PaginationHelper from '@utils/helpers/pagination';

import { CreateMovieInput } from '@dtos/movie/create-movie.dto';
import { UpdateMovieInput } from '@dtos/movie/update-movie.dto';
import { QueryMovieInput } from '@dtos/queries/movie-query-input.dto';
import { ParamsInput } from '@dtos/params/params-input.dto';

import { handleErrors } from '@errors/handle-errors';
import { RateMovieInput } from '@dtos/movie/rate-movie.dto';

class Controller {
 public async create(req: Request, res: Response) {
  try {
   const data = req.body as CreateMovieInput;

   const movie = await Service.create(data);

   return res.status(201).json(movie);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findAll(req: Request, res: Response) {
  try {
   const {
    page = undefined,
    limit = undefined,
    actors,
    directors,
    genres,
    search = undefined,
   } = req.query as QueryMovieInput;

   const movies = await Service.findAll(
    page ? Number(page) : page,
    limit ? Number(limit) : limit,
    actors ? (actors as string[]) : [],
    directors ? (directors as string[]) : [],
    genres ? (genres as string[]) : [],
    search,
   );

   return res
    .status(200)
    .json(
     page && limit
      ? PaginationHelper.paginate(movies, page, limit)
      : { data: movies?.data },
    );
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findById(req: Request, res: Response) {
  try {
   const { id } = req.params as ParamsInput;

   const movie = await Service.findByField('id', id);

   return res.status(200).json(movie);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findAllDirectors(req: Request, res: Response) {
  try {
   const directors = await Service.findAllDirectors();

   return res.status(200).json(directors);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async update(req: Request, res: Response) {
  try {
   const data = req.body as UpdateMovieInput;

   const movie = await Service.update(req.params.id, data);

   return res.status(200).json(movie);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async softDelete(req: Request, res: Response) {
  try {
   const movie = await Service.softDelete(req.params.id);

   return res.status(200).json(movie);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async rate(req: Request, res: Response) {
  try {
   const { id } = req.params as ParamsInput;
   const userId = req.auth?.id as string;
   const { rate } = req.body as RateMovieInput;
   const movie = await Service.rate(id, userId, rate);

   return res.status(200).json(movie);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
