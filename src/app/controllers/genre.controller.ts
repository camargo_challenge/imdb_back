import { Request, Response } from 'express';

import Service from '@services/genre.service';

import { handleErrors } from '@errors/handle-errors';
import { QueryMovieInput } from '@dtos/queries/movie-query-input.dto';

class Controller {
 public async findAll(req: Request, res: Response) {
  try {
   const { onlyWithMovies = false } = req.query as QueryMovieInput;
   const genres = await Service.findAll(onlyWithMovies);

   return res.status(200).json(genres);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findById(req: Request, res: Response) {
  try {
   const { id } = req.params;
   const genre = await Service.findById(id);

   return res.status(200).json(genre);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
