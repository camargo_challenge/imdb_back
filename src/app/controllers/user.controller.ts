import { Request, Response } from 'express';

import Service from '@services/user.service';

import PaginationHelper from '@utils/helpers/pagination';

import { CreateAccountInput } from '@dtos/user/create-account.dto';
import { UpdateAccountInput } from '@dtos/user/update-account.dto';
import { QueryInput } from '@dtos/queries/user-query-input.dto';
import { ParamsInput } from '@dtos/params/params-input.dto';

import { handleErrors } from '@errors/handle-errors';

class Controller {
 public async create(req: Request, res: Response) {
  try {
   const data = req.body as CreateAccountInput;

   const user = await Service.create(data);

   return res.status(201).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findAll(req: Request, res: Response) {
  try {
   const {
    page = 1,
    limit = 10,
    status,
    role,
    search,
   } = req.query as QueryInput;

   const users = await Service.findAll(+page, +limit, status, role, search);

   return res.status(200).json(PaginationHelper.paginate(users, +limit, +page));
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async findById(req: Request, res: Response) {
  try {
   const { id } = req.params as ParamsInput;

   const user = await Service.findByField('id', id);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async update(req: Request, res: Response) {
  try {
   const data = req.body as UpdateAccountInput;

   const user = await Service.update(req.params.id, data);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async softDelete(req: Request, res: Response) {
  try {
   const user = await Service.softDelete(req.params.id);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
