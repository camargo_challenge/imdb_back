import { Request, Response } from 'express';

import Service from '@services/user.service';

import { UpdateAccountInput } from '@dtos/user/update-account.dto';
import { handleErrors } from '@errors/handle-errors';

class Controller {
 public async findById(req: Request, res: Response) {
  try {
   const id = req.auth?.id as string;
   const user = await Service.findByField('id', id);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async update(req: Request, res: Response) {
  try {
   const data = req.body as UpdateAccountInput;
   const id = req.auth?.id as string;

   const user = await Service.update(id, data);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }

 public async softDelete(req: Request, res: Response) {
  try {
   const id = req.auth?.id as string;
   const user = await Service.softDelete(id);

   return res.status(200).json(user);
  } catch (error: any) {
   handleErrors(error);
  }
 }
}

export default new Controller();
